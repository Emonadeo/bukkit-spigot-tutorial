package com.emonadeo.tutorial;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener
{
	
	private FileConfiguration config = this.getConfig();
	
	@Override
	public void onEnable()
	{
		System.out.println("Hi");
		this.getServer().getPluginManager().registerEvents(this, this);
		
		//Config
		config.options().copyDefaults(true);
		this.saveConfig();
		this.reloadConfig();
	}
	
	@Override
	public void onDisable()
	{
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(command.getName().equalsIgnoreCase("health"))
		{
			if(sender instanceof Player)
			{
				Player p = (Player)sender;
				double health = p.getHealth();
				p.sendMessage("Dein Leben: " + config.getString("Farbe").replace('&', '�') + health);
				return true;
			}
		}
		return false;
	}
	
	@EventHandler
	public void onKnochenBlitz (PlayerInteractEvent e)
	{
		Player p = e.getPlayer();
		if(p.getItemInHand().getType() == Material.BONE)
		{
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK)
			{
				Block b = e.getClickedBlock();
				p.getWorld().strikeLightning(b.getLocation());
			}
		}
	}
}
